package com.devsuperior.bds03.controllers.exceptions;

import com.devsuperior.bds03.services.exceptions.DatabaseException;
import com.devsuperior.bds03.services.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<StandardError> entityNotFound(ResourceNotFoundException e, HttpServletRequest request) {
        var status = HttpStatus.NOT_FOUND;
        var error = getStandardError(status, "Resource not found", e.getMessage(), request);

        return ResponseEntity.status(status).body(error);
    }

    @ExceptionHandler(DatabaseException.class)
    public ResponseEntity<StandardError> databaseException(DatabaseException e, HttpServletRequest request) {
        var status = HttpStatus.BAD_REQUEST;
        var error = getStandardError(status, "Database exception", e.getMessage(), request);

        return ResponseEntity.status(status).body(error);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationError> methodArgumentNotValidException(MethodArgumentNotValidException e,
                                                                           HttpServletRequest request) {
        ValidationError error = new ValidationError(Instant.now(), HttpStatus.UNPROCESSABLE_ENTITY.value(),
                                                    "Validation exception", e.getMessage(), request.getRequestURI());

        for (FieldError f : e.getBindingResult().getFieldErrors()) {
            error.addError(f.getField(), f.getDefaultMessage());
        }

        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<StandardError> constraintViolationException(ConstraintViolationException e,
                                                                      HttpServletRequest request) {
        ValidationError error = new ValidationError(Instant.now(), HttpStatus.UNPROCESSABLE_ENTITY.value(),
                                                    "Validation exception", e.getMessage(), request.getRequestURI());

        e.getConstraintViolations().forEach(message -> error.addError(message.getPropertyPath().toString(),
                                                                      message.getMessageTemplate()));

        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);
    }

    private StandardError getStandardError(HttpStatus status, String message, String exceptionMessage,
                                           HttpServletRequest request) {
        return new StandardError(Instant.now(), status.value(), message, exceptionMessage, request.getRequestURI());
    }

}
